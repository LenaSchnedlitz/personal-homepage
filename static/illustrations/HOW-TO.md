# How to Add Illustrations

- Download from https://undraw.co with primary color #bbb
- If padding is needed, extend viewbox by 100px in each direction
- Replace fills with classes (will be scripted soon!):
  - #2f2e41 -> 'illustration--darkest'
  - #3f3d56 -> 'illustration--dark'
  - #cacaca, #ccc -> 'illustration--grey'
  - #e6e6e6, #e6e7e8, #e4e4e4 -> 'illustration--light'
  - #f2f2f2, #f0f0f0 -> 'illustration--lightest'
  - #fff -> 'illustration--white'
  - #bbbbbb -> 'illustration--accent'
  - #fd6584 -> 'illustration--accent2'
  - #9e616a -> 'illustration--skin-dark'
  - #ffb8b8 -> 'illustration--skin-light'
